SRC=main.c log.c info.c
OUT=memdbg

.PHONY:debug
debug:
	gcc -g -Wall -Wextra -fsanitize=leak,address,undefined $(SRC) -o $(OUT) -DZ0_DEBUG

.PHONY:demo
demo:
	gcc -g ./demo/main.c -o demo1

.PHONY:clear
clear:
	rm $(OUT)
