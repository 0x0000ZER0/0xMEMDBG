#include "defs.h"

#include <stdarg.h>
#include <stdio.h>

void
log_dbg(const char *fmt, ...)
{
#ifdef Z0_DEBUG
	printf("DBG: ");

	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	printf(".\n");
#endif
}

void
log_err(const char *fmt, ...)
{
	fprintf(stderr, "ERR: ");

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	fprintf(stderr, ".\n");
}

