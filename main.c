#include "defs.h"
#include <string.h>

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		log_err("need to specify the process name");
		return 1;
	}
	
	char *name;
	name = argv[1];

	size_t name_len;
	name_len = strlen(argv[1]);

	uint32_t res;

	proc_info info;
	res = proc_info_init(&info, name, name_len);
	if (res != PROC_INFO_INIT_OK) {
		log_err("couldn't find the PID (%u)", res);
		return 1;
	}

	log_dbg("pid [%d]", info.pid);
	log_dbg("mem [%s]", info.mem);
	log_dbg("map [%s]", info.map);

	res = proc_info_attach(&info);
	if (res != PROC_INFO_ATTACH_OK)
		log_err("couldn't attach to the process");

	res = proc_info_addr(&info);
	if (res != PROC_INFO_ADDR_OK)
		log_err("couldn't get the addresses (%u)", res);

	log_dbg("heap  [%p]-[%p]", info.heap_start, info.heap_end);
	log_dbg("stack [%p]-[%p]", info.stack_start, info.stack_end);

	proc_info_detach(&info);
	proc_info_free(&info);

	return 0;
}
