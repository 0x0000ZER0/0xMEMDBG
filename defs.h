#ifndef DEFS_H
#define DEFS_H

#include <unistd.h>
#include <stdint.h>

enum {
	PROC_INFO_INIT_OK = 0,
	PROC_INFO_INIT_ERR_OPEN_DIR,
	PROC_INFO_INIT_ERR_CLOSE_DIR,
	PROC_INFO_INIT_ERR_OPEN_FILE,
	PROC_INFO_INIT_ERR_READ_FILE,
	PROC_INFO_INIT_ERR_NOT_FOUND,
	PROC_INFO_INIT_ERR_ALLOC
};

enum {
	PROC_INFO_ATTACH_OK = 0,
	PROC_INFO_ATTACH_ERR_INIT,
	PROC_INFO_ATTACH_ERR_WAIT
};

enum {
	PROC_INFO_ADDR_OK = 0,
	PROC_INFO_ADDR_ERR_OPEN,
	PROC_INFO_ADDR_ERR_READ,
	PROC_INFO_ADDR_ERR_ALLOC,
};

enum {
	PROC_INFO_DETACH_OK = 0,
	PROC_INFO_DETACH_ERR,
};

enum {
	PROC_INFO_FREE_OK = 0,
	PROC_INFO_FREE_ERR
};

typedef struct {
	char  *mem;
	char  *map;
	pid_t pid;

	uintptr_t heap_start;
	uintptr_t heap_end;
	uintptr_t stack_start;
	uintptr_t stack_end;
} proc_info;

void
log_dbg(const char*, ...);

void
log_err(const char*, ...);

uint32_t
proc_info_init(proc_info*, const char*, size_t);

uint32_t
proc_info_attach(proc_info*);

uint32_t
proc_info_addr(proc_info*);

uint32_t
proc_info_detach(proc_info*);

uint32_t
proc_info_free(proc_info*);

#endif
