#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>
#include <malloc.h>

static uintptr_t
to_ptr(char *txt, size_t len)
{
	char tmp;

	uintptr_t ptr;
	ptr = 0;
	
	for (size_t i = 0; i < len; ++i) {
		tmp = (txt[i] >> 6) | ((txt[i] >> 3) & 0x8);
		tmp = (txt[i] & 0xF) + tmp;
		ptr = (ptr << 4) | (uintptr_t)tmp;
	}

	return ptr;
}

static bool
is_num(char *str, size_t len)
{
	for (size_t i = 0; i < len; ++i)
		if (str[i] < '0' || str[i] > '9')
			return false;

	return true;
}

uint32_t
proc_info_init(proc_info *info, const char *name, size_t name_len)
{
	info->heap_start  = 0x0;
	info->heap_end	  = 0x0;
	info->stack_start = 0x0;
	info->stack_end	  = 0x0;

	const char proc_path[] = "/proc/";
	const char status_path[] = "/status";
	const char mem_path[] = "/mem";
	const char map_path[] = "/maps";
	const char name_key[] = "Name:";

	DIR *dir;
	dir = opendir(proc_path);
	if (dir == NULL)
		return PROC_INFO_INIT_ERR_OPEN_DIR;

	char file_path[128];
	char file_buff[256];

	size_t off;
	size_t dname_len;

	struct dirent *entry;
	entry = readdir(dir);
	while (entry != NULL) {
		if (memcmp(entry->d_name, ".", 1) == 0 ||
		    memcmp(entry->d_name, "..", 2) == 0) {
			entry = readdir(dir);
			continue;
		}

		if (entry->d_type == DT_DIR) {
			dname_len = strlen(entry->d_name);

			if (is_num(entry->d_name, dname_len)) {
				off = 0;
				memcpy(file_path + off, proc_path, sizeof (proc_path) - 1);	
				off += sizeof (proc_path) - 1;
				memcpy(file_path + off, entry->d_name, dname_len);
				off += dname_len;
				memcpy(file_path + off, status_path, sizeof (status_path) - 1); 
				off += sizeof (status_path) - 1;

				file_path[off] = '\0';

				int file;
				file = open(file_path, O_RDONLY);
				if (file == -1) {
					closedir(dir);
					return PROC_INFO_INIT_ERR_OPEN_FILE;
				}

				ssize_t bytes;
				bytes = read(file, file_buff, sizeof (file_buff));
				if (bytes == -1) {
					close(file);
					closedir(dir);
					return PROC_INFO_INIT_ERR_READ_FILE;
				}
				
				if (memcmp(file_buff, name_key, sizeof (name_key) - 1) == 0) {
					char *curr;	
					curr = file_buff + sizeof (name_key) - 1;
					while (*curr == ' ' || *curr == '\t')
						++curr;
				
					char *end;
					end = curr;
					while (*end != '\n')
						++end;

					off = end - curr + sizeof (name_key);
					file_buff[off] = '\0';
		
					if (memcmp(curr, name, name_len) == 0) {
						info->pid = 0;
						for (size_t i = 0; i < dname_len; ++i) {
							info->pid *= 10;
							info->pid += entry->d_name[i] - '0';
						}

						off = sizeof (proc_path) + sizeof (mem_path) + dname_len - 1;
						info->mem = malloc(off);
						if (info->mem == NULL) {
							close(file);
							closedir(dir);
							return PROC_INFO_INIT_ERR_ALLOC;							
						}

						off = 0;
						memcpy(info->mem + off, proc_path, sizeof (proc_path) - 1);	
						off += sizeof (proc_path) - 1;
						memcpy(info->mem + off, entry->d_name, dname_len);
						off += dname_len;
						memcpy(info->mem + off, mem_path, sizeof (mem_path) - 1); 
						off += sizeof (mem_path) - 1;
						
						info->mem[off] = '\0';

						off = sizeof (proc_path) + sizeof (map_path) + dname_len - 1;
						info->map = malloc(off);
						if (info->map == NULL) {
							free(info->mem);
							close(file);
							closedir(dir);
							return PROC_INFO_INIT_ERR_ALLOC;							
						}

						off = 0;
						memcpy(info->map + off, proc_path, sizeof (proc_path) - 1);	
						off += sizeof (proc_path) - 1;
						memcpy(info->map + off, entry->d_name, dname_len);
						off += dname_len;
						memcpy(info->map + off, map_path, sizeof (map_path) - 1); 
						off += sizeof (map_path) - 1;
						
						info->map[off] = '\0';

						close(file);
						closedir(dir);
						return PROC_INFO_INIT_OK;
					}
				}

				close(file);
			}
		}

		entry = readdir(dir);
	}
	

	int res;
	res = closedir(dir);
	if (res != 0)
		return PROC_INFO_INIT_ERR_CLOSE_DIR;

	return PROC_INFO_INIT_ERR_NOT_FOUND;
}

uint32_t
proc_info_attach(proc_info *info)
{
	long res;
	res = ptrace(PTRACE_ATTACH, info->pid, NULL, NULL);
	if (res == -1)
		return PROC_INFO_ATTACH_ERR_INIT;

	int status;
	if (waitpid(info->pid, &status, 0) == -1 || !WIFSTOPPED(status))
		return PROC_INFO_ATTACH_ERR_WAIT;

	return PROC_INFO_ATTACH_OK;
}

uint32_t
proc_info_addr(proc_info *info)
{
	// NOTE: proc files are virtual, 
	//       which is why we can't get the size in advance.

	int file_map;
	file_map = open(info->map, O_RDONLY);
	if (file_map == -1)
		return PROC_INFO_ADDR_ERR_OPEN;

	const size_t buff_max = 2048;

	size_t buff_len;
	buff_len = 0;

	char *buff;
	buff = malloc(buff_max);
	if (buff == NULL) {
		close(file_map);
		return PROC_INFO_ADDR_ERR_ALLOC;
	}

	ssize_t bytes;
	do {
		bytes = read(file_map, buff, buff_max);
		if (bytes == -1) {
			free(buff);
			close(file_map);
			return PROC_INFO_ADDR_ERR_READ;
		}
		
		buff_len += (size_t)bytes;

		char *new_buff;
		new_buff = realloc(buff, buff_len + buff_max);
		if (new_buff == NULL) {
			free(buff);
			close(file_map);
			return PROC_INFO_ADDR_ERR_ALLOC;
		}
		buff = new_buff;
	} while ((size_t)bytes > buff_max);

	close(file_map);	

	const char   path_heap[]  = "[heap]";
	const char   path_stack[] = "[stack]";
	const off_t  line_off	  = 73;
	const size_t addr_size	  = 12;
	// address                   perm offset   dev   inode                      path
	// ------------------------- ---- -------- ----- -------------------------- ------
	// 55f9019ce000-55f901ea7000 rw-p 00000000 00:00 0                          [heap]
	// 55f9019ce000-55f901ea7000 rw-p 00000000 00:00 0                          [stack]
	// (25)                      (4)  (8)      (5)   (26)
	size_t off;
	for (size_t i = 0; i < buff_len; ++i) {
		if (buff[i] != '[')
			continue;
	
		int cmp;	
		
		cmp = memcmp(&buff[i], path_heap, sizeof (path_heap) - 1);
		if (cmp == 0) {
			off = 0;
			info->heap_start = to_ptr(&buff[i - line_off + off], addr_size);
			off += addr_size + 1;
			info->heap_end = to_ptr(&buff[i - line_off + off], addr_size);
			
			continue;
		}

		cmp = memcmp(&buff[i], path_stack, sizeof (path_stack) - 1);
		if (cmp == 0) {
			off = 0;
			info->stack_start = to_ptr(&buff[i - line_off + off], addr_size);
			off += addr_size + 1;
			info->stack_end = to_ptr(&buff[i - line_off + off], addr_size);
			
			break;
		}
	}

	free(buff);
	return PROC_INFO_ADDR_OK;
}

uint32_t
proc_info_detach(proc_info *info)
{
	long res;
	res = ptrace(PTRACE_DETACH, info->pid, NULL, NULL);
	if (res != 0) {
		return PROC_INFO_DETACH_ERR;
	}

	return PROC_INFO_DETACH_OK;
}

uint32_t
proc_info_free(proc_info *info)
{
	free(info->map);
	free(info->mem);
	return PROC_INFO_FREE_OK;
}
